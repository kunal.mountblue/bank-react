import React, {Component} from 'react';
import './App.css';
import GetBanks from './components/GetBanks';
import MainHeader from './components/MainHeader';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageSize: 20
    }
  }
  render(){
    return (
      <div className="App">
        <MainHeader/>
        <GetBanks pageSize={this.state.pageSize}></GetBanks>
      </div>
    );
  }
}
export default App;
