import React, {Component} from 'react';
import fetch from 'node-fetch';
import "antd/dist/antd.css";
import ShowAllBanks from './ShowAllBanks';
import Pagination from './Pagination';

class GetBanks extends Component {
    constructor(props){
        super(props);
        this.state = {
            loaded: false,
            banks: [],
            searchWord: '',
            currentPage: 1,
            resultsPerPage: props.pageSize,
        };
    }
    componentDidMount() {
        this.fetchBanks();
    }
    fetchBanks = () => {
        console.log('fetchBanks()');
        fetch(`https://vast-shore-74260.herokuapp.com/banks?city=MUMBAI`)
            .then(data => data.json())
            .then(data => {
                this.setState({
                    loaded:true,
                    banks:data
                })
            })
            .catch( (e) => {
                console.log(e);
            });
    }
    switchPage = (val) => {
        this.setState({
            currentPage: val
        });
    }
    
    handleSearch = (event) => {
        this.setState({ searchWord: event.target.value });
    }
    render() {
        const loaded = this.state.loaded;
        const data = this.state.banks.filter(bank => {
           return (bank.ifsc.toLowerCase().includes(this.state.searchWord.toLowerCase()) ||
           bank.bank_id.toString().includes(this.state.searchWord) ||
           bank.branch.toLowerCase().includes(this.state.searchWord.toLowerCase()) ||
           bank.address.toLowerCase().includes(this.state.searchWord.toLowerCase()) ||
           bank.city.toLowerCase().includes(this.state.searchWord.toLowerCase()) ||
           bank.district.toLowerCase().includes(this.state.searchWord.toLowerCase()) ||
           bank.state.toLowerCase().includes(this.state.searchWord.toLowerCase()) || 
           bank.bank_name.toLowerCase().includes(this.state.searchWord.toLowerCase())
           );
        });
        const currentPage = this.state.currentPage;
        const resultsPerPage = this.state.resultsPerPage;
        
        const lastPageIndex = currentPage * resultsPerPage;
        const firstPageIndex = lastPageIndex - resultsPerPage;
        const currentResult = data.slice(firstPageIndex, lastPageIndex);

        return (
            <div>  
                <label>
                    Search
                    <input type="text" value={this.state.searchWord} name="searchWord" onChange={this.handleSearch}/>
                </label>
                {
                    loaded ? (
                        <React.Fragment>
                            <ShowAllBanks data={currentResult} />
                            <Pagination 
                                resultsPerPage={resultsPerPage}
                                totalPages={data.length}
                                switchPage={this.switchPage}
                            />
                        </React.Fragment> 
                    ) 
                    : (<h1>Loading...</h1>)
                }
            </div>  
        );
    }   
}
export default GetBanks;