import React, {Component} from 'react';
import '../App.css';

class ShowAllBanks extends Component{
    render(){
        return(
            <table className="table">
                <thead className="thead-dark">
                    <tr>
                        <th>IFSC</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Branch</th>
                        <th>City</th>
                        <th>District</th>
                        <th>State</th>
                    </tr>
                </thead>
                
                { 
                    this.props.data.map(val => {
                        return (
                            <tbody>
                                <tr>
                                    <td>{val.ifsc}</td>
                                    <td>{val.bank_id}</td>
                                    <td>{val.bank_name}</td>
                                    <td>{val.branch}</td>
                                    <td>{val.city}</td>
                                    <td>{val.district}</td>
                                    <td>{val.state}</td>
                                </tr>
                            </tbody>
                        ); 
                    })   
                }
            </table>
        );
    }
}
export default ShowAllBanks;