import React, { Component } from 'react';
import '../index.css';

class MainHeader extends Component{
    render() {
        return(
            <div  className="main-header">
                <h1 id="main-header-text">Bank Dataset</h1>
            </div>
        );
    }
}

export default MainHeader;