import React, { Component } from 'react'

class Pagination extends Component {
    constructor(props){
        super(props);
        this.state = {
            previousStage: 0,
            nextStage: 20
        };
    }
    
    previousPage = event => {
        event.preventDefault();
        if (this.state.previousStage > 0) {
            this.setState({
                previousStage: this.state.previousStage - 1,
                nextStage: this.state.nextStage - 1
            });
        }
        if(this.state.nextStage > 20){
            this.props.switchPage(this.state.nextStage - 1);
        }
    };
  
    nextPage = event => {
        event.preventDefault();
        this.setState({
            previousStage: this.state.previousStage + 1,
            nextStage: this.state.nextStage + 1,
        });
        this.props.switchPage(this.state.nextStage + 1);
    };

    firstPage = event => {
        event.preventDefault();
        this.props.switchPage(1);
    }
  
    render() {
        const pages = [];
        for (let i = 1; i <= Math.ceil(this.props.totalPages / this.props.resultsPerPage); i++) {
            pages.push(i);
        }
    
        const paging = pages.slice(this.state.previousStage, this.state.nextStage);

        return (
            <nav>
                <ul className="pagination">
                    <li className="page-item">
                        <button onClick={this.firstPage} >
                            First
                        </button>
                    </li>

                    <li className="page-item">
                        <button onClick={this.previousPage}>
                            Previous
                        </button>
                    </li>
        
                    {paging.map(pageNo => (
                        <li key={pageNo} className="page-item">
                            <button onClick={() => this.props.switchPage(pageNo)}>
                                {pageNo}
                            </button>
                        </li>
                    ))}
                    <li className="page-item">
                        <button onClick={this.nextPage}>
                            Next
                        </button>
                    </li>

                    <li className="page-item">
                        <button onClick={this.lastPage}>
                            Last    
                        </button>
                    </li>
                </ul>
            </nav>
        );
    }
}
export default Pagination;